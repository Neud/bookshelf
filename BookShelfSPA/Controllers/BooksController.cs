﻿using BookShelfSPA.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace BookShelfSPA.Controllers
{
    public class BooksController : ApiController
    {
        private static List<Book> books = BookContainer.Books;

        // GET: api/Books
        public IEnumerable<Book> GetBooks()
        {
            return books;
        }

        // PUT: api/Books/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutBook(int id, Book newBook)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != newBook.Id)
            {
                return BadRequest();
            }

            var oldBook = books.FirstOrDefault(b => b.Id == id);

            if (oldBook == null)
            {
                return NotFound();
            }

            oldBook.Update(newBook);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Books
        [ResponseType(typeof(Book))]
        [HttpPost]
        public IHttpActionResult PostBook(Book book)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            book.Id = books.Select(b => b.Id).Max() + 1;
            books.Add(book);

            return CreatedAtRoute("DefaultApi", new { id = book.Id }, book);
        }

        // DELETE: api/Books/5
        [ResponseType(typeof(Book))]
        public IHttpActionResult DeleteBook(int id)
        {
            var book = books.FirstOrDefault(b => b.Id == id);

            if (book == null)
            {
                return NotFound();
            }

            books.Remove(book);

            return Ok(book);
        }
    }
}