﻿using System.Web.Mvc;

namespace BookShelfSPA.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}