﻿using BookShelfSPA.Helpers;
using BookShelfSPA.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace BookShelfSPA.Controllers
{
    public class ImagesController : ApiController
    {
        private static List<Book> books = BookContainer.Books;

        // PUT: api/Images/5
        public byte[] GetImage(int id)
        {
            var book = books.FirstOrDefault(b => b.Id == id);
            if (book != null)
            {
                return book.Image;
            }

            return null;
        }

        // PUT: api/Images/5
        [ResponseType(typeof(void))]
        public async Task<HttpResponseMessage> PutImage(int id)
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var provider = await Request.Content.ReadAsMultipartAsync(new InMemoryMultipartFormDataStreamProvider());

            var files = await provider.GetFiles();

            if ((files.Count() == 1) && (files[0] != null))
            {
                var file = files[0];
                if (file.Size != 0)
                {
                    var book = books.FirstOrDefault(b => b.Id == id);
                    if (book != null)
                    {
                        book.Image = new MemoryStream(file.Data).ToArray();
                        return new HttpResponseMessage(HttpStatusCode.Accepted);
                    }

                    return new HttpResponseMessage(HttpStatusCode.InternalServerError);
                }
            }

            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }
    }
}