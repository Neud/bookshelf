﻿using BookShelfSPA.Helpers;
using System.ComponentModel.DataAnnotations;

namespace BookShelfSPA.ValidationAttributes
{
    public class ValidISBN : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return false;
            }

            return Isbn.TryValidate(value.ToString());
        }
    }
}