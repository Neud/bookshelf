﻿using System.Collections.Generic;
using System.IO;
using System.Web;

namespace BookShelfSPA.Models
{
    public static class BookContainer
    {
        public static List<Book> Books = new List<Book>
        {
            new Book
            {
                Id = 1,
                Title = "C# 6.0 Cookbook",
                PageNumber = 704,
                PublicationYear = 2015,
                PublisherName = "O'Reilly Media",
                ISBN = "1491921463",
                Image = File.ReadAllBytes(Path.Combine(HttpContext.Current.Server.MapPath("~"), "Content\\Images\\cookbook.jpg")),
                Authors = new List<Author>
                {
                    new Author { FirstName = "Jay", LastName = "Hilyard" },
                    new Author { FirstName = "Stephen", LastName = "Teilhet" }
                }
            },
            new Book
            {
                Id = 2,
                Title = "C# 6.0 in a Nutshell",
                PageNumber = 1136,
                PublicationYear = 2015,
                PublisherName = "O'Reilly Media",
                ISBN = "1491927062",
                Image = File.ReadAllBytes(Path.Combine(HttpContext.Current.Server.MapPath("~"), "Content\\Images\\nutshell.jpg")),
                Authors = new List<Author>
                {
                    new Author { FirstName = "Joseph", LastName = "Albahari" },
                    new Author { FirstName = "Ben", LastName = "Albahari" }
                }
            },
            new Book
            {
                Id = 3,
                Title = "JavaScript: The Good Parts",
                PageNumber = 176,
                PublicationYear = 2008,
                PublisherName = "O'Reilly Media",
                ISBN = "0596517742",
                Image = File.ReadAllBytes(Path.Combine(HttpContext.Current.Server.MapPath("~"), "Content\\Images\\javascript.jpg")),
                Authors = new List<Author>
                {
                    new Author { FirstName = "Douglas", LastName = "Crockford" }
                }
            }
        };
    }
}