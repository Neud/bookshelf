﻿using System.ComponentModel.DataAnnotations;

namespace BookShelfSPA.Models
{
    public class Author
    {
        [Required(ErrorMessage = "Введите имя")]
        [MaxLength(20, ErrorMessage = "Имя должно быть длиной не более 20 символов")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Введите фамилию")]
        [MaxLength(20, ErrorMessage = "Фамилия должна быть длиной не более 20 символов")]
        public string LastName { get; set; }
    }
}