﻿namespace BookShelfSPA.Models
{
    public class SortButton
    {
        public string Text { get; set; }
        public string PropertyName { get; set; }
    }
}