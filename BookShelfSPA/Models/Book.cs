﻿using BookShelfSPA.ValidationAttributes;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace BookShelfSPA.Models
{
    public class Book
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Введите заголовок")]
        [MaxLength(30, ErrorMessage = "Заголовок должен быть длиной не более 30 символов")]
        public string Title { get; set; }

        [EnsureMinimumElements(1, ErrorMessage = "У книги должен быть хотя бы 1 автор")]
        public IEnumerable<Author> Authors { get; set; }

        [Required(ErrorMessage = "Введите количество страниц")]
        [Range(0, 10000, ErrorMessage = "Число страниц должно быть не более 10000")]
        public int PageNumber { get; set; }

        [MaxLength(40, ErrorMessage = "Название издательства должно быть длиной не более 30 символов")]
        public string PublisherName { get; set; }

        [Required(ErrorMessage = "Введите год публикации")]
        [Range(1800, int.MaxValue, ErrorMessage = "Год публикации должен быть не меньше 1800")]
        public int PublicationYear { get; set; }

        [Required(ErrorMessage = "ISBN")]
        [ValidISBN(ErrorMessage = "Введите валидный ISBN")]
        public string ISBN { get; set; }

        public byte[] Image { get; set; }

        public HttpPostedFileBase Attachment { get; set; }

        public void Update(Book newBook)
        {
            Title = newBook.Title;
            PageNumber = newBook.PageNumber;
            PublisherName = newBook.PublisherName;
            PublicationYear = newBook.PublicationYear;
            ISBN = newBook.ISBN;
            Authors = newBook.Authors;
        }
    }
}