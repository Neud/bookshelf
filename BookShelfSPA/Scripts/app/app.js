﻿// Sorry for the mess. First Angular project without manual
(function (angular) {
    'use strict';
    angular.module('BookApp', ['ngCookies'])
    .controller('BookCtrl', ['$scope', '$http', 'orderByFilter', '$cookieStore', function ($scope, $http, orderBy, $cookieStore) {
        $scope.loading = true;
        $scope.addMode = false;

        // List of authors for a new book
        $scope.newbookAuthors = [{ FirstName: '', LastName: '' }];

        // Default sorting settings
        $scope.propertyName = $cookieStore.get('sortPropertyName');
        $scope.reverse = !$cookieStore.get('sortReverse');

        // Table sorting
        $scope.sortBy = function (propertyName) {
            $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName)
                ? !$scope.reverse : false;
            $cookieStore.put('sortReverse', $scope.reverse)

            $scope.propertyName = propertyName;
            $cookieStore.put('sortPropertyName', $scope.propertyName);

            $scope.booksOrdered = orderBy($scope.books, $scope.propertyName, $scope.reverse);
        };

        // Initial table loading
        $http.get('/api/Books/').success(function (data) {
            $scope.books = data;
            $scope.sortBy($scope.propertyName);
            $scope.loading = false;
        })
        .error(function () {
            $scope.error = "При загрузке книг произошла ошибка!";
            $scope.loading = false;
        });

        // Toggle 'Edit existing book' mode
        $scope.toggleEdit = function () {
            this.book.editMode = !this.book.editMode;

            if (this.book.editMode) {
                if ($scope.editedBook !== undefined) {
                    $scope.editedBook.editMode = false;
                    $scope.editedBook.imageMode = false;
                }

                $scope.editError = null;
                $scope.addMode = false;
                $scope.editedBook = this.book;
            } else {
                $scope.editedBook = null;
            }
        };

        // Toggle 'Load image for a book' mode
        $scope.toggleImage = function () {
            this.book.imageMode = !this.book.imageMode;

            if (this.book.imageMode) {
                if ($scope.editedBook !== undefined) {
                    $scope.editedBook.editMode = false;
                    $scope.editedBook.imageMode = false;
                }

                $scope.editError = null;
                $scope.addMode = false;
                $scope.editedBook = this.book;
            } else {
                $scope.editedBook = null;
            }
        };

        // Toggle 'Add new book' mode
        $scope.toggleAdd = function () {
            $scope.addMode = !$scope.addMode;

            if ($scope.addMode) {
                $scope.addError = null;
            }

            if ($scope.editedBook !== undefined) {
                $scope.editedBook.editMode = false;
                $scope.editedBook.imageMode = false;
            }
        };

        // Append new empty author to the list of authors
        $scope.addNewAuthor = function (authors) {
            var newItemNo = authors.length + 1;
            authors.push({ 'id': 'Authors' + newItemNo });
        };

        // Remove author from the list of authors
        $scope.removeAuthor = function (authors, authorIndex) {
            authors.splice(authorIndex, 1);
        };

        // Save changes in existing book
        $scope.save = function () {
            $scope.loading = true;

            var book = this.book;

            $http.put('/api/Books/' + book.Id, book).success(function (data) {
                book.editMode = false;
                $scope.loading = false;
                $scope.editError = null;
            }).error(function (data) {
                $scope.editError = 'При сохранении изменений произошла ошибка!';

                for (var name in data.ModelState) {
                    $scope.editError += '\r\n' + data.ModelState[name]
                }

                $scope.loading = false;
            });
        };

        // Convert request data to upload image file
        $scope.getModelAsFormData = function (data) {
            var dataAsFormData = new FormData();
            angular.forEach(data, function (value, key) {
                dataAsFormData.append(key, value);
            });
            return dataAsFormData;
        };

        // Upload an image for the book
        $scope.saveImage = function () {
            $scope.loading = true;

            var book = this.book;

            $http({
                url: '/api/Images/' + book.Id,
                method: "PUT",
                data: $scope.getModelAsFormData({ Attachment: book.Attachment }),
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            }).success(function (data) {
                book.imageMode = false;
                $scope.editError = null;

                $http.get('/api/Images/' + book.Id).success(function (data) {
                    book.Image = data;
                    $scope.loading = false;
                })
                .error(function () {
                    $scope.error = "При обновлении изображения произошла ошибка!";
                    $scope.loading = false;
                });
            }).error(function (data) {
                $scope.editError = 'При загрузке изображения произошла ошибка!';

                for (var name in data.ModelState) {
                    $scope.editError += '\r\n' + data.ModelState[name]
                }

                $scope.loading = false;
            });
        };
 
        // Add new book
        $scope.add = function () {
            $scope.loading = true;

            var newbook = this.newbook;
            newbook.Authors = $scope.newbookAuthors;

            $http.post('/api/Books/', newbook).success(function (data) {
                $scope.addMode = false;
                $scope.books.push(data);
                $scope.sortBy(null);
                $scope.loading = false;
                $scope.addError = null;
                $("form[name='addBook'] input[type=text], form[name='addBook'] input[type=number]").val("");
            }).error(function (data) {
                $scope.addError = 'При добавлении новой книги произошла ошибка!';

                for (var name in data.ModelState) {
                    $scope.addError += '\r\n' + data.ModelState[name]
                }
                
                $scope.loading = false;
            });
        };

        // Delete existing book
        $scope.deletebook = function () {
            $scope.loading = true;
            var bookid = this.book.Id;
            $http.delete('/api/Books/' + bookid).success(function (data) {
                $.each($scope.books, function (i) {
                    if ($scope.books[i].Id === bookid) {
                        $scope.books.splice(i, 1);
                        return false;
                    }
                });
                $scope.sortBy(null);
                $scope.loading = false;
                $scope.error = null;
            }).error(function (data) {
                $scope.error = 'При удалении книги произошла ошибка!';

                for (var name in data.ModelState) {
                    $scope.error += '\r\n' + data.ModelState[name]
                }

                $scope.loading = false;
            });
        };
    }])
    .directive("akFileModel", ["$parse",
        function ($parse) {
            return {
                restrict: "A",
                link: function (scope, element, attrs) {
                    var model = $parse(attrs.akFileModel);
                    var modelSetter = model.assign;
                    element.bind("change", function () {
                        scope.$apply(function () {
                            modelSetter(scope, element[0].files[0]);
                        });
                    });
                }
            };
        }]);
})(window.angular);